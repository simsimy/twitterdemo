﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Couchbase.Configuration;
using Couchbase;
namespace twitterdemo.Models
{
    public  static class CocuchBaseDb
    {
        public static CouchbaseClient Client = null;
        static CocuchBaseDb()
        {
            CouchbaseClientConfiguration config = new CouchbaseClientConfiguration();
            config.Urls.Add(new Uri("http://127.0.0.1:8091/pools"));
            config.Bucket = "default";
            Client = new CouchbaseClient(config);
        }
    }
}