﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using twitterdemo.Models;

namespace twitterdemo.Controllers
{
    public class TweeterApiController : ApiController
    {
        protected CouchbaseRepository<Tweet> tweetRep = new CouchbaseRepository<Tweet>();
        protected CouchbaseRepository<User> userRep = new CouchbaseRepository<User>();
        protected CouchbaseRepository<TweetList> userTweetRep = new CouchbaseRepository<TweetList>();
        protected CouchbaseRepository<FollowerList> followers = new CouchbaseRepository<FollowerList>();
        protected CouchbaseRepository<UserName> userNames = new CouchbaseRepository<UserName>();
    }
}