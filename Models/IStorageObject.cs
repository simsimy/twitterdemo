﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace twitterdemo.Models
{
    public interface IStorageObject
    {
        string Id { get; set; }
    }
}