﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace twitterdemo.Models
{
    [Serializable]
    [JsonObject]
    public class Tweet : IStorageObject
    {
        [JsonProperty]
        public string Id { get; set; }
        [JsonProperty]
        public string UserId { get; set; }
        [JsonProperty]
        public string UserName { get; set; }
        [JsonProperty]
        public string Text { get; set; }
        [JsonProperty]
       // [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime PostedDate { get; set; }
        public Tweet() { this.Id = Guid.NewGuid().ToString(); this.PostedDate = DateTime.UtcNow; }
    }
}