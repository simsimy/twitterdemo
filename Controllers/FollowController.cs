﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using twitterdemo.Models;
namespace twitterdemo.Controllers
{
    public class FollowController : TweeterApiController
    {
        [Serializable]
        [JsonObject]
        public class FlowingInfo
        {
            [JsonProperty]
            public string Followed { get; set; }
            [JsonProperty]
            public string Following { get; set; }
        }
        public HttpResponseMessage Post(Guid id, [FromBody]Guid value)
        {
            User user = userRep.Get(id.ToString());
            if (user == null) return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "No matching user found");
            User followedUser = userRep.Get(value.ToString());
            if (followedUser == null) return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "No matching user found to follow");
            followers.Create(new FollowerList() { Id = user.Follows });
            followers.UpdateOptimistic(user.Follows, t =>
            {
                if (!t.Followers.Contains(followedUser.Id)) t.Followers.Add(followedUser.Id);
                return t;
            });
            return Request.CreateResponse<FlowingInfo>(HttpStatusCode.Created, new FlowingInfo() { Followed = followedUser.Id, Following = user.Id });
        }


        public HttpResponseMessage Delete(Guid id, [FromBody]Guid value)
        {
            User user = userRep.Get(id.ToString());
            if (user == null) return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "No matching user found");
            User followedUser = userRep.Get(value.ToString());
            if (followedUser == null) return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "No matching user found to follow");
            followers.Create(new FollowerList() { Id = user.Follows });
            followers.UpdateOptimistic(user.Follows, t =>
            {
                if (t.Followers.Contains(followedUser.Id)) t.Followers.Remove(followedUser.Id);
                return t;
            });
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }
    }
}
