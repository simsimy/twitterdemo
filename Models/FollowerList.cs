﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace twitterdemo.Models
{
    [Serializable]
    [JsonObject]
    public class FollowerList : IStorageObject
    {
        [JsonProperty]
        public string Id { get; set; }
        [JsonProperty]
        public List<string> Followers { get; set; }
        public FollowerList() { this.Followers = new List<string>(); }
    }
}