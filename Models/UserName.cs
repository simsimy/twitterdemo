﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace twitterdemo.Models
{
    [Serializable]
    [JsonObject]
    public class UserName : IStorageObject
    {
        [JsonIgnore]
        public string Id { get { return Name; } set { Name = value; } }
        [JsonProperty]
        public string Name { get; set; }
        [JsonProperty]
        public string UserId { get; set; }
    }
}