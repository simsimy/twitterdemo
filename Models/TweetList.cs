﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace twitterdemo.Models
{
    [Serializable]
    [JsonObject]
    public class TweetList : IStorageObject
    {
        [JsonProperty]
        public string Id { get; set; }
        [JsonProperty]
        public List<Tweet> Tweets { get; set; }
        public TweetList() { this.Tweets = new List<Tweet>(); }
    }
}