﻿using Enyim.Caching.Memcached;
using Enyim.Caching.Memcached.Results;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace twitterdemo.Models
{
    public class CouchbaseRepository<T> : IDisposable where T : IStorageObject
    {
        
        public bool Create(T item)
        {
            var res = CocuchBaseDb.Client.ExecuteStore(StoreMode.Add, item.Id, item);
            return (res.Success);
        }
        public virtual bool UpdateOptimistic(string Id, Func<T, T> updateFunc)
        {

            var success = false;
            T documentToReturn = default(T);

            while (!success)
            {

                IGetOperationResult<T> item = CocuchBaseDb.Client.ExecuteGet<T>(Id);
                if (item == null)
                {
                    return false;
                }

                T result = (T) item.Value;
                
                // pass the latest document to the given block so the latest changes can be applied
                if (updateFunc == null) return true;
              result=  updateFunc(result);

              
                var storeResult = CocuchBaseDb.Client.ExecuteCas(StoreMode.Replace, Id, result,
                                                    item.Cas);
                if (!storeResult.Success)
                    continue;

                documentToReturn = result;
                success = true;
            }

            return success;
        }
        public virtual bool UpdatePesimistic(string Id, Func<T, T> updateFunc, TimeSpan? lockDuration=null)
        {
            if (!lockDuration.HasValue)lockDuration = TimeSpan.FromSeconds(10);
            var success = false;
            T documentToReturn = default(T);

            while (!success)
            {

                IGetOperationResult<T> item = CocuchBaseDb.Client.ExecuteGetWithLock<T>(Id, lockDuration.Value);
                if (item == null)
                {
                    return false;
                }

                T result = (T)item.Value;

                // pass the latest document to the given block so the latest changes can be applied
                if (updateFunc == null) return true;
                result = updateFunc(result);


                var storeResult = CocuchBaseDb.Client.ExecuteCas(StoreMode.Replace, Id, result,
                                                    item.Cas);
                if (!storeResult.Success)
                    continue;

                documentToReturn = result;
                success = true;
            }

            return success;
        }
        public T Get(string id)
        {
            return (T)CocuchBaseDb.Client.Get(id);
        }

        public void Dispose() { } 
    }
}