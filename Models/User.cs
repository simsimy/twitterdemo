﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace twitterdemo.Models
{
    [Serializable]
    [JsonObject]
    public class User : IStorageObject
    {
        public static string FollowAll = "follow_all";
        public static string FollowsPrfix = "follows_";
        public static string RecentTweetsPrefix = "recent_";
        public static string ArchivedTweetsPrefix = "archive_";

        [JsonProperty]
        public string Id { get; set; }
        [JsonProperty]
        public string Name { get; set; }
        [JsonIgnore]
        public string Follows { get { return User.FollowsPrfix + Id; } }
        [JsonIgnore]
        public string RecentTweets
        {
            get
            {
                return User.RecentTweetsPrefix + Id;
            }
        }
        [JsonIgnore]
        public string ArchiveTweets
        {
            get
            {
                return User.ArchivedTweetsPrefix + Id;
            }
        }
        public User()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        public User(string userName)
            : this()
        {
            this.Name = userName;
        }
    }
}