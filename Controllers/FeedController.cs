﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using twitterdemo.Models;

namespace twitterdemo.Controllers
{
    public class FeedController : TweeterApiController
    {
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse<List<Tweet>>(HttpStatusCode.OK, GetTweetes(twitterdemo.Models.User.FollowAll));
        }

        // GET api/values/5
        public HttpResponseMessage Get(Guid id)
        {
            User user = userRep.Get(id.ToString());
            if (user == null) return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "No matching user found");
            return Request.CreateResponse<List<Tweet>>(HttpStatusCode.OK, GetTweetes(user.Follows));
        }
        private List<Tweet> GetTweetes(string from)
        {
            ConcurrentBag<List<Tweet>> objectList = new ConcurrentBag<List<Tweet>>();
            FollowerList fl = followers.Get(from);
            if (fl == null) return new List<Tweet>();
            Parallel.ForEach(fl.Followers, s =>
            {
                TweetList tRecent = userTweetRep.Get(Models.User.RecentTweetsPrefix + s);
                TweetList tArchive= userTweetRep.Get(Models.User.ArchivedTweetsPrefix + s);
                if (tRecent != null && tRecent.Tweets.Any()) objectList.Add(tRecent.Tweets);
                if (tArchive != null && tArchive.Tweets.Any()) objectList.Add(tArchive.Tweets);
            });
            SortedList<DateTime, Tweet> tweets = new SortedList<DateTime, Tweet>();
            foreach (List<Tweet> tweetList in objectList)
            {
                foreach (Tweet tw in tweetList) tweets.Add(tw.PostedDate, tw);
            }
            return tweets.Values.ToList();
        }
    }
}
