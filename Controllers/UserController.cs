﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using twitterdemo.Models;

namespace twitterdemo.Controllers
{
    public class UserController : TweeterApiController
    {
        // POST api/user
        public HttpResponseMessage Post([FromBody]string value)
        {
            
            if (string.IsNullOrEmpty(value)) return Request.CreateResponse<string>( HttpStatusCode.BadRequest, "No user was provided");
            value = value.ToLowerInvariant();
            User u = new User(value);
            bool unique = false;
            UserName un = new UserName() { Name = value, UserId = u.Id };
           

                unique = userNames.Create(un);



                if (!unique)
                {
                    un = userNames.Get(un.Id);
                    return Request.CreateResponse<UserName>(HttpStatusCode.Conflict, un);
                }
                else
                {
                    
                        userRep.Create(u);
                        followers.Create(new FollowerList() { Id = Models.User.FollowAll });
                        followers.UpdateOptimistic(twitterdemo.Models.User.FollowAll, t =>
                        {
                            if (!t.Followers.Contains(u.Id)) t.Followers.Add(u.Id);
                            return t;
                        });
                    

                }
            return Request.CreateResponse<User>(HttpStatusCode.Created, u);
        }
    }
}
