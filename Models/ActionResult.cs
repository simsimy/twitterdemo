﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace twitterdemo.Models
{
  
    public class ActionResult<T>
    {
        public HttpStatusCode Status { get; set; }
        public string Result { get; set; }
        public T Item { get; set; }

        public static ActionResult<T> GetResult(HttpStatusCode code, string text, T result)
        {
            return new ActionResult<T>() { Status = code, Result = text, Item = result };
        }
        public HttpResponseMessage GetMessage(HttpRequestMessage request)
        {
            if (EqualityComparer<T>.Default.Equals(Item, default(T))) return request.CreateResponse<string>(this.Status, this.Result);
            return request.CreateResponse<T>(this.Status, this.Item);
        }
    }
}