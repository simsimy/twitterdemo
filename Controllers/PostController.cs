﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using twitterdemo.Models;

namespace twitterdemo.Controllers
{
    public class PostController : TweeterApiController
    {
  
        // POST api/user
        [HttpPost]
        public HttpResponseMessage Post(Guid id, [FromBody]string MessageText)
        {

            User user = userRep.Get(id.ToString());
            if (user == null) return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "No matching user found");
            Tweet tweet = new Tweet() {  Text = MessageText, UserName = user.Name, UserId = user.Id };
            userTweetRep.Create(new TweetList() { Id = user.RecentTweets });
            userTweetRep.UpdateOptimistic(user.RecentTweets, t =>
                {
                    if (!t.Tweets.Any(s => s.Id == tweet.Id)) t.Tweets.Add(tweet);
                    return t;
                });

            return Request.CreateResponse<Tweet>(HttpStatusCode.Created,tweet);

        }
    }
}
